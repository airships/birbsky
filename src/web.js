import cfg from "../config.js"
import Fastify from "fastify" // https://fastify.dev/docs/latest/Reference/
import FastifyStatic from "@fastify/static"
import fs from "fs"
import { db } from "./db.js"
import { addNew, stats } from "./manage.js"
import { pathJoin, readFileCached, parseHost, response } from "./utils.js"

const fastify = Fastify({
  // trustProxy: true, // while it does provide correct ips behind a reverce proxy, it messes up the hostnames
  logger: true,
})

fastify.register(FastifyStatic, {
  root: pathJoin(cfg.static),
  prefix: "/",
})


fastify.addHook("preHandler", (req, reply, done) => {
  const [osub, odomain] = parseHost(req.headers["origin"])
  if (odomain && req.url.startsWith("/api/")) {
    const [sub, domain] = parseHost(req.hostname)
    if (domain && sub === cfg.subdomain) {
      reply.header("Access-Control-Allow-Origin", req.headers["origin"])
      reply.header("Access-Control-Allow-Methods", "GET, POST")
      reply.header("Access-Control-Allow-Headers", "*")
    }
  }
  if (req.method === "OPTIONS")
    return reply.send()
  done()
})

// show page
fastify.get("/", async (req, reply) => {
  reply.type("text/html")
  let indexfn = pathJoin(cfg.static, `${req.hostname}.html`)
  let did
  const [sub, domain] = parseHost(req.hostname)
  if (!fs.existsSync(indexfn)) {
    if (!sub || sub === "www")
      indexfn = pathJoin(cfg.static, `${domain}.html`)
    else if (!cfg.subdomain || sub !== cfg.subdomain) {
      const rec = await db.findOne({ domain, sub })
      if (rec) {
        if (rec.banned)
          return reply.send("Banned hostname")
        did = rec.did
        indexfn = pathJoin(cfg.static, "index-user.html")
        // return reply.redirect(304, `https://bsky.app/profile/${req.hostname}`)
      }
    }
    if ((cfg.subdomain && sub === cfg.subdomain) || !fs.existsSync(indexfn))
      indexfn = pathJoin(cfg.static, "index.html")
  }
  let html = readFileCached(indexfn)
  // limitation: currently, cfg.subdomain must be defined in config or some things won't work
  const svchost = domain ? `${cfg.subdomain}.${domain}` : domain
  html = html
    .replace(/###SERVICE###/g, svchost)
    .replace(/###HANDLE###/g, req.hostname)
    .replace(/###DID###/g, did || "")
  // TODO: inject a tiny popup for custom pages maybe?
  reply.send(html)
})

// reply to bsky
fastify.get("/.well-known/atproto-did", async (req, reply) => {
  const [sub, domain] = parseHost(req.hostname)
  const na = "Not registered"
  if (!sub)
    return reply.send(na)
  const rec = await db.findOne({ domain, sub })
  const did = rec ? rec.did || na : na
  reply.send(did)
})

// list domains
fastify.get("/api/domains", async (req, reply) => {
  const [_, domain] = parseHost(req.hostname)
  reply.send({
    success: true,
    current: cfg.domains.includes(domain) ? domain : null,
    domains: stats.domains,
  })
})

// register a subdomain
fastify.post("/api/register", async (req, reply) => {
  const res = await addNew({
    ...req.body,
    domain: req.body.domain || parseHost(req.hostname)[1],
  })
  reply.send(res)
})

// check a subdomain
fastify.post("/api/check", async (req, reply) => {
  const domain = req.body.domain ? req.body.domain.trim().toLowerCase() : parseHost(req.hostname)[1]
  if (!cfg.domains.includes(domain))
    return reply.send(response("This domain is not available", false))
  const sub = req.body.sub ? req.body.sub.trim().toLowerCase() : null
  if (!sub)
    return reply.send(response("Subdomain must be specified", false))
  const user = await db.findOne({ $and: [
    { domain },
    { sub },
  ] })
  reply.send({
    success: true,
    domain,
    sub,
    free: !user,
    banned: user ? !!user.banned : false,
    restricted: user ? !!user.restrict : false,
  })
})

// list domains
// while this is public information (domain refers to DID on ATProto), we're still going to cover it with an optional password
fastify.get("/api/users", async (req, reply) => {
  if (cfg.api_password && req.query.password !== cfg.api_password)
    return reply.send(response("Protected area", false))
  const users = await db.find({ $and: [
    { did: { $ne: "" } },
    { did: { $ne: null } },
    { did: { $exists: true } },
    { restrict: { $ne: true } },
    { banned: { $ne: true } },
  ] })
  reply.send({
    success: true,
    users: users.reduce((res, user) => ({
      ...res,
      [user.did]: [
        ...(res[user.did] || []),
        `${user.sub}.${user.domain}`,
      ],
    }), {}),
  })
})


export async function init() {
  try {
    await fastify.listen({ port: cfg.port, host: cfg.host })
  } catch (fire) {
    fastify.log.error(fire)
    throw fire
  }
}
