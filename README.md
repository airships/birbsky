# Birbsky

Free Bluesky handle hosting.  
More stuff planned, extending it to not just a handle provider but to something more!

Example: [bsky.shy.cat](https://bsky.shy.cat)

## Features

* Allows users to specify current handle instead of DID
* Validates used handles on Bluesky
* Shows a page with links to the user profile, when accessing by handle's URL
* Can work on multiple domains and handle subdomain exclusions
* Works under any front-end webserver or by its own
* No stand-alone database required

## Setup

```bash
git clone <this repo>
cd <dir>
npm i
```

Before running, you should copy `config.example.js` to `config.js` and modify it to suit your needs.
Most notably, you should specify service Bluesky handle and its app password (not account password).

Then you can run it with:

```bash
node ./index.js
```

## Guide

### Default pages

If you want to serve pages from Birbsky, just add a `<hostname>.html` to `static` directory, along with asset files. This works for subdomains as well.
For example, for `https://example.com` domain file should be named `example.com.html`, and `test.example.com.html` for `https://test.example.com`.

For user's subdomains Birbsky will always serve `index-user.html` instead to better reflect the domain purpose and to prevent confusion.

To sum up, here's a priority sequence for subdomains (first one wins):

1. `test.example.com.html`
2. `index-user.html` if user record exists
3. `example.com.html`
4. `index.html`

### Subdomain restrictions

You should specify restrictions for domains that are in use to avoid both unreachable handles and possible confusion.

### DNS configuration

Add `A` or `CNAME` wildcard record `*` pointed to Birbsky IP or host (`@`) respectively. It doesn't matter if there are other non-wildcard records - they will not be affected.  
If you plan to serve website root as well, `@` record should point to Birbsky as well.

### Front-end web server configuration

While Birbsky can work on its own, it is always recommended to cover it behind a front-end server.

Here's nginx config example:

```nginx
server {
  listen 443 ssl;
  listen [::]:443 ssl;
  server_name
    .example.com
    .example.org
  ;
  ssl_certificate /var/cert/main.pem;
  ssl_certificate_key /var/cert/main.key;
  access_log /var/log/nginx/birbsky-access.log;
  error_log /var/log/nginx/birbsky-error.log;
  error_page 502 =502 /502.html;
  location = /502.html {
    root /var/www/default;
  }
  location / {
    proxy_pass http://127.0.0.1:5200;
    proxy_redirect off;
    proxy_http_version 1.1;
    proxy_pass_request_headers on;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Host $server_name;
  }
}
```

## TODO

This list covers current state of things, excluding milestones.

* Passwords (per domain)
* Handle ban system
* Code injection on static webpages
* User validation system for third party services
* Cache minimal user profiles (upon checking)
* Display name and avatar on personal pages
* A page listing all users

## License

MIT
© 2023-2024, Snowly
([owlso.me](https://owlso.me))
