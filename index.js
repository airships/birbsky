import * as app from "./src/index.js"

async function run() {
  try {
    console.log("DB init...")
    await app.initdb()
    console.log("Bluesky init...")
    await app.initbsky()
    console.log("Manager init...")
    await app.initmanage()
    console.log("Starting the server...")
    await app.initweb()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

run()
