import cfg from "../config.js"
import { db, buildRecord } from "./db.js"
import * as bsky from "./bsky.js"
import { enqueue, schedule, response, now } from "./utils.js"

const re = {
  did: /^did:[a-z]+:[a-zA-Z0-9._:%-]*[a-zA-Z0-9._-]$/,
  sub: /^[a-z\d_]([a-z\d\-\._]*[a-z\d_])?$/,
}

export const stats = {
  users: 0,
  domains: {},
}
const statsReset = {
  users: () => stats.users = 0,
  domains: () => stats.domains = cfg.domains.reduce((res, domain) => ({ ...res, [domain]: 0 }), {}),
}

export async function addNew({ domain = "", sub = "", handle = "" }) {
  // small sanitization
  domain = domain.trim().toLowerCase()
  sub = sub.trim().toLowerCase()
  handle = handle.trim()

  // require everything
  if (!domain || !sub || !handle) {
    return response("All fields must be specified", false)
  }

  // sanity checks
  if (!cfg.domains.includes(domain))
    return response("Specified domain is not available", false)
  if (sub.length > 50 || !re.sub.test(sub))
    return response("Subdomain name is invalid", false)
  if (handle.length > 100)
    return response("DID or handle is invalid", false)
  if (handle === cfg.bsky_login || handle === bsky.own.did)
    return response("Hey, that's me :)", false)

  // check if restricted
  const item = await db.findOne({ domain, sub })
  if (item) {
    if (item.restrict)
      return response("This subdomain cannot be used", false)
    return response("This subdomain is already registered", false)
  }

  return await enqueue(addNewJob, { domain, sub, handle_or_did: handle })
}

async function addNewJob({ domain, sub, handle_or_did }) {
  // check profile and get DID
  const profile = await bsky.getProfile(handle_or_did)
  if (!profile)
    return response("Profile not found", false)
  const did = profile.did
  if (cfg.bsky_follow) {
    // check if blocked
    if (profile.viewer && profile.viewer.blockedBy)
      return response(`Please unblock ${cfg.bsky_login} account on BlueSky`, false)
    // follow the user
    try {
      await bsky.follow(did)
    } catch (err) {
      console.error(err)
      return response("Bluesky error happened :(", false)
    }
  }
  // add record
  await db.insert(buildRecord({ domain, sub, did }))
  return response("Successfully registered")
}

// TODO: this is getting out of paw, rewrite
async function checkExisting(onlyStats = false) {
  console.log("Checking existing users...")
  const all = await db.find({ $and: [
    { did: { $ne: "" } },
    { did: { $ne: null } },
    { did: { $exists: true } },
    // { timestamp: { $lt: curtime - cfg.subdomain_timeout } }, // don't, because we also check if Bluesky still has this user
    { restrict: { $ne: true } },
    { banned: { $ne: true } },
  ] })
  if (all.length === 0) {
    statsReset.users()
    statsReset.domains()
    return
  }
  all.sort((a, b) => a.timestamp - b.timestamp)
  stats.domains = cfg.domains.reduce((res, domain) => ({
    ...res,
    [domain]: all.filter(usr => usr.domain === domain).length,
  }), {})
  let check = all.reduce((res, usr) => res.includes(usr.did) ? res : [...res, usr.did], [])
  stats.users = check.length
  if (onlyStats)
    return
  check = check.slice(0, 25) // ATProto getProfiles limit is 25
  let profiles = await bsky.getProfile(check)
  if (!profiles) {
    console.warn(`Error getting profiles for ${check.length} users`)
    return
  }
  profiles = profiles.reduce((res, profile) => ({ ...res, [profile.did]: profile }), {})
  const curtime = now()
  const upd = [], rem = []
  for (const { _id, did, domain, sub, timestamp } of all.filter(usr => check.includes(usr.did))) {
    // if user account still exists
    if (!(did in profiles)) {
      console.log(`Bluesky cannot find DID "${did}" (${sub}.${domain}). Removing...`)
      rem.push(_id)
      continue
    }
    // if blocked (how rude!)
    if (cfg.bsky_block_remove && profiles[did].viewer && profiles[did].viewer.blockedBy) {
      console.log(`User ${sub}.${domain} (${did}) blocked me. Removing...`)
      rem.push(_id)
      continue
    }
    // if handle is in use
    if (profiles[did].handle.toLowerCase() === `${sub}.${domain}`) {
      upd.push(_id)
      continue
    }
    // if timed out, remove the record
    if (timestamp + cfg.subdomain_timeout < curtime) {
      console.log(`Removing ${sub}.${domain} (${did})...`)
      rem.push(_id)
      continue
    }
    // FIXME: when nothing to do, the record stays on top for the next check. need to handle this later on
  }
  if (upd.length > 0)
    await db.update({ _id: { $in: upd } }, { $set: { timestamp: curtime } }, { multi: true })
  if (rem.length > 0)
    await db.remove({ _id: { $in: rem } }, { multi: true })
  console.log(`Checked ${all.length} user records: ${upd.length} updated, ${rem.length} removed`)
}

async function checkFollows() {
  console.log("Checking existing follows...")
  const all = (await db.find({ $and: [
    { did: { $ne: "" } },
    { did: { $ne: null } },
    { did: { $exists: true } },
  ] })).map(user => user.did)
  for (const did of bsky.own.follows) {
    if (did === bsky.own.did)
      continue
    if (!all.includes(did)) {
      await bsky.unfollow(did)
    // } else {
    //   await bsky.follow(did)
    }
  }
}

export async function init() {
  await checkExisting(true)
  schedule.always(cfg.check_interval, async () => {
    await checkExisting()
    if (cfg.bsky_follow)
      await checkFollows()
  })
}
