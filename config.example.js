export default {
  // db filename
  db: "nest.db",
  // db backup directory name
  db_backup: "backups",
  // how often we should update a backup and run DB compaction operation
  db_maintenance_interval: 60 * 60,

  // web port and host
  port: 5200,
  host: "127.0.0.1",

  // static files location that will be served to the web
  static: "static",
  // index.html will be used for all domains
  // example.com.html will be used on root domains only (if file exist)

  // bluesky config
  bsky_service: "https://bsky.social", // PDS URL, no need to change this
  bsky_login: "bsky.example.com",
  bsky_password: "qwer-asdf-zxcv-1234", // app password (not account)

  // if registered users should be followed by this account
  bsky_follow: false,

  // remove user if they blocked this account
  bsky_block_remove: true,

  // how often to check records and compare them to Bluesky
  check_interval: 30 * 60,

  // domains to provide to the public, shown in order
  domains: ["example.com", "example.org"],
  // subdomain for the service itself
  subdomain: "bsky",
  subdomain_restrictions: {
    // restrict specified subdomains to be used on all domains
    all: ["www", "ns", "ns1", "ns2", "_acme-challenge"],
    // additionally, restrict specified subdomains to be used on specified domains
    "example.com": ["admin", "secure"],
  },
  // after what time unused records should be released
  subdomain_timeout: 24 * 60 * 60,

  /// simple flood prevention
  /// note: throttle will work during a flood only, in normal operation requests will be processed immediately
  // max requests that can be queued before showing "too many requests" error
  queue_max: 100,
  // start counter before the throttle will hit (number will lower down with every new item in queue)
  queue_throttle_start: 20,
  // when mid is hit, mid ms delay will be added to every item
  queue_throttle_mid: 10, // can be the same as start
  queue_throttle_mid_ms: 100,
  // when counter will reach zero, end ms delay will be added
  queue_throttle_end_ms: 500,

  /// protect somewhat-sensitive areas (optional)
  // notes:
  // - we don't store nor process non-public information, which is why security here is not that important
  // - this password might be transferred in plain text
  // protected endpoints can be found in web.js
  //api_password: "secrets",
}
