import cfg from "../config.js"
import Datastore from "nestdb" // https://github.com/JamesMGreene/nestdb
import { pathJoin, schedule, now } from "./utils.js"
import { copyFileSync, mkdirSync, existsSync } from "fs"

class DatastoreAsync extends Datastore {
  constructor(...args) {
    super(...args)
    const that = this
    const promisify = ["load", "count", "find", "findOne", "insert", "update", "remove"]
    for (const name of promisify) {
      const func = this[name]
      this[name] = (...args) => new Promise((resolve, reject) => {
        func.call(that, ...args, (err, resp) => {
          if (err)
            reject(err)
          else
            resolve(resp)
        })
      })
    }
  }
}

export const db = new DatastoreAsync({
  filename: cfg.db,
})

export async function init() {
  if (!existsSync(pathJoin(cfg.db_backup)))
    mkdirSync(pathJoin(cfg.db_backup))

  await db.load()

  // add initial records
  // TODO: handle removed domains and subdomain_restrictions
  const prev = (await db.find({ restrict: true }))
    .reduce((res, user) => ({ ...res, [user.domain]: [...(res[user.domain] || []), user.sub] }), {})
  const add = []
  for (const domain of cfg.domains) {
    const subs = [
      ...cfg.subdomain_restrictions.all,
      ...(cfg.subdomain_restrictions[domain] || [])
    ].filter(sub => !prev[domain] || !prev[domain].includes(sub))
    if (subs.length === 0)
      continue
    add.push(...subs.map(
      sub => buildRecord({ domain, sub, restrict: true })
    ))
  }
  if (add.length > 0)
    await db.insert(add)

  schedule.always(cfg.db_maintenance_interval, dbMaint)
}

export function buildRecord({ domain, sub, did = "", restrict = undefined }) {
  return {
    domain, sub, did,
    restrict,
    timestamp: now(),
  }
}

export async function dbMaint() {
  const destfn = `${new Date().toISOString().substring(0, 10)}.db`
  console.log(`Backing up DB to ${destfn}...`)
  copyFileSync(pathJoin(cfg.db), pathJoin(cfg.db_backup, destfn))
  await new Promise(resolve => db.persistence.compactDatafile(resolve))
}
