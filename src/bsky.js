import cfg from "../config.js"
import { BskyAgent } from "@atproto/api" // https://www.npmjs.com/package/@atproto/api
import { db } from "./db.js"

const bsky = new BskyAgent({ service: cfg.bsky_service })

const devdid = "did:plc:bnco5qywhi6jlpwi5w5mjmsv" // owlso.me :3

export const own = {
  did: "",
  follows: [], // DID array
  followUris: [], // AT URI array
}

export async function init() {
  await bsky.login({
    identifier: cfg.bsky_login,
    password: cfg.bsky_password,
  })
  const profile = await getProfile(cfg.bsky_login)
  own.did = profile.did
  if (cfg.subdomain) {
    await db.update(
      { sub: cfg.subdomain },
      { $set: { did: own.did, restrict: true } },
      { multi: true }
    )
    const subs = cfg.domains.map(domain => `${cfg.subdomain}.${domain}`)
    if (!subs.includes(profile.handle))
      console.log(`Registered handles for myself: ${subs.join(" ")}`)
  }
  if (cfg.bsky_follow)
    await updateFollows()
}

async function updateFollows() {
  // own.follows = (await bsky.getFollows({ actor: own.did })).data.follows.map(follow => follow.did)
  // ...but because we need to have AT URI for each record to unfollow, we need to access CRUD instead
  const follows = (await bsky.app.bsky.graph.follow.list({
    repo: own.did,
    collection: "app.bsky.graph.follow",
  })).records
  own.follows = follows.map(record => record.value.subject)
  own.followUris = follows.map(record => record.uri)
  const devidx = own.follows.indexOf(devdid)
  if (devidx > -1) {
    own.follows.splice(devidx, 1)
    own.followUris.splice(devidx, 1)
  } else {
    await bsky.follow(devdid)
  }
  return own.follows
}

export async function follow(did) {
  if (own.follows.includes(did))
    return
  const resp = await bsky.follow(did)
  own.follows.push(did)
  own.followUris.push(resp.uri)
}

export async function unfollow(did) {
  const idx = own.follows.indexOf(did)
  if (idx === -1)
    return
  await bsky.deleteFollow(own.followUris[idx]) // wth, Bluesky???
  own.follows.splice(idx, 1)
  own.followUris.splice(idx, 1)
}

export async function getProfile(dids) {
  try {
    let resp
    if (Array.isArray(dids)) {
      resp = await bsky.getProfiles({ actors: dids }) // current limit: 25
      resp = resp.success ? resp.data.profiles : null
    } else {
      resp = await bsky.getProfile({ actor: dids })
      resp = resp.success ? resp.data : null
    }
    return resp
  } catch (fire) { console.log(fire) }
  return null
}
